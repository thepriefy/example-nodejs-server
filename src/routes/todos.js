import { todos as controller } from "@controllers";

export function setupRoutes(router) {
  router.get("/", controller.list).post("/", controller.create);
}
