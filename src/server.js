import fs from "fs";
import path from "path";
import express from "express";
import mongoose from "mongoose";
import bodyParser from "body-parser";

import config from "../config";

const app = express();

// app.get("/", (req, res) => {
//   res.send("Hello");
// });

function setupDatabase() {
  return mongoose.connect(
    config.databaseUrl,
    { useNewUrlParser: true }
  );
}

function setupRoutes() {
  // list files in controller dorectory
  const files = fs.readdirSync("./src/routes"); // ['todos.js']

  // loop through files
  files.map(file => {
    const router = express.Router(); // init router for each route
    const resourceName = file.split(".")[0]; // todos

    import(path.join(__dirname, "routes", resourceName))
      .then(module => {
        module.setupRoutes(router);
      })
      .catch(err => {
        console.log(err);
      });

    app.use(`/${resourceName}`, router);
  });
}

export function setup() {
  app.use(bodyParser.urlencoded({ extended: true }));
  app.use(bodyParser.json());

  setupRoutes();

  setupDatabase().then(() => {
    app.listen(config.port, () => {
      console.log(`listening to port ${config.port}...`);
    });
  });
}
