require("dotenv").config();
export default {
  port: process.env.PORT || 80,
  databaseUrl: process.env.DATABASE_URL
};
